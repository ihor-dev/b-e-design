
$(document).ready(function(){

	var $menu = $(".fmenu");
		 
	$(window).scroll(function(){

		if ( $(this).scrollTop() > 60 && $menu.hasClass("default")){
			$menu.fadeOut(600,function(){
				$(this).removeClass("default")
					.addClass("fixed-menu")
					.fadeIn(400);
			});
		} else if($(this).scrollTop() <= 2 && $menu.hasClass("fixed-menu")) {
			$menu.fadeOut(600,function(){
				$(this).removeClass("fixed-menu")
					.addClass('default')
					.fadeIn(400);
			});
		}
	});//scroll
});
